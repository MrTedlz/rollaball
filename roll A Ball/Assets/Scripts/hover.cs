﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hover : MonoBehaviour
{
    float speed;
    private bool dirRight = true;
    private Vector3 location;
    private Vector3 location2;
    private float newX;
    private float newZ;
    private Vector3 newLocation;

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(0.5f, 0.8f);
         newX = Random.Range(-5.0f, 10.0f);
         newZ = Random.Range(-5.0f, 10.0f);
         newLocation = new Vector3(newX, 0.0f, newZ);
        location = transform.position;
        location2 = transform.position + newLocation;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(location, location2, Mathf.PingPong(Time.time * speed, 1.0f));
        transform.Rotate(Vector3.up * 10f * Time.deltaTime);
        
    }
   

}
